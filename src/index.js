/**
 * singular server file
 * 
 * /admin handles website for GUI interfacing
 * /node handles dwn calls
 */

const http = require('http');
const {renderHtml} = require('./admin.js')

const respond = (req, res) => {
  console.log(req.url)
  switch (req.url.split('/')[1]) {
    case 'admin': 
      res.writeHead(200)
      res.end(renderHtml())
      break
    case 'node':
      res.writeHead(200)
      res.end('{node stuff}')
      break
    default: 
      res.writeHead(404)
      res.end()
      break
  }

}

const main = () => {
  const server = http.createServer(respond);
  server.listen(3000);
}

main()